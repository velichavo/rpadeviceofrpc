﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace RpcClientTest
{
    public class MessageRequest
    {
        private Guid id;
        private TypeCommand command;
        private Dictionary<string, int> param;
        private string crc;

        public Guid Id { get => id; set => id = value; }
        public TypeCommand Command { get => command; set => command = value; }
        public Dictionary<string, int> Param { get => param; set => param = value; }
        public string Crc { get => crc; set => crc = value; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="guid"> ID устройства(регистрируется заранее)</param>
        /// <param name="command">Команда которую будет выполнять устройство(лежит в enum TypeCommand)</param>
        /// <param name="param">парамтры команды(например для комманды Turn у чайника можно задать температуру нагрева воды)</param>
        public MessageRequest(string guid, TypeCommand command, Dictionary<string, int> param)
        {
            Crc = GetHashCode().ToString();
            Id = new Guid(guid);
            Id.ToString();
            Param = param;
            Command = command;
        }
    }

    public class GetStatusResponse
    {
        private TypeDevice tDevice;
        private Guid id;
        private Dictionary<string, int> param;

        public GetStatusResponse()
        {

        }
        public Guid Id { get => id; set => id = value; }
        public TypeDevice TDevice { get => tDevice; set => tDevice = value; }
        public Dictionary<string, int> Param { get => param; set => param = value; }
    }

    public enum TypeDevice
    {
        Device,
        Lamp,
        Kettle
    }

    public enum TypeCommand
    {
        GetStatus,
        Turn
    }
}
