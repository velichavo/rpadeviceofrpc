﻿using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Threading.Tasks;

namespace RpcClientTest
{
    class Program
    {
        static void Main(string[] args)
        {
            //var Lamp = new MainDevice("6c8485b4-f0b2-4f81-b6d7-3b1d87799d15", TypeDevice.Lamp);
            //var Kettle = new MainDevice("6c8485b4-f0b2-4f81-b6d7-3b1d87799d16", TypeDevice.Kettle);

            Dictionary<string, int> d = new Dictionary<string, int>();
            //d.Add("Temperature", 95);
            MessageRequest mr = new MessageRequest("6c8485b4-f0b2-4f81-b6d7-3b1d87799d16", TypeCommand.GetStatus, d);
            string json = JsonSerializer.Serialize(mr, new JsonSerializerOptions() { WriteIndented = true });

            var rpcM = new RpcModule(json);
            string response = rpcM.Run();

            Console.WriteLine("     Response: " + response);
            Console.WriteLine(" Press [enter] to exit.");
            Console.ReadLine();
        }
    }
}
