﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RpcClientTest
{
    public class RpcModule
    {
        private readonly string message;
        public RpcConnection rpcClient;
        private string response = null;
        public Task TaskCall;
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="json">Строка в формате JSON</param>
        public RpcModule(string json)
        {
            rpcClient = new RpcConnection(this);
            message = json;
        }
        public async Task InvokeAsync()
        {
            response = await rpcClient.CallAsync(message);
        }
        /// <summary>
        /// Стартует запрос
        /// </summary>
        /// <returns>Возвращает строку с ответом в формате JSON </returns>
        public string Run()
        {
            TaskCall = InvokeAsync();
            TaskCall.Wait();
            rpcClient.Close();
            return response;
        }
    }
}
