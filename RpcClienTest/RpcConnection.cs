﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Concurrent;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RpcClientTest
{
    public class RpcConnection
    {
        private const string QueueName = "rpc_queue";

        public RpcModule RpcModule;
        private readonly IConnection _connection;
        private readonly IModel _channel;
        private readonly string _replyQueueName;
        private readonly EventingBasicConsumer _consumer;

        private readonly ConcurrentDictionary<string, TaskCompletionSource<string>>
            _callbackMapper = new ConcurrentDictionary<string, TaskCompletionSource<string>>();

        public RpcConnection(RpcModule rpcModule)
        {
            RpcModule = rpcModule;
            var factory = new ConnectionFactory() { HostName = "localhost" };
            factory.UserName = "tar";
            factory.Password = "1234";
            _connection = factory.CreateConnection();
            _channel = _connection.CreateModel();
            _replyQueueName = _channel.QueueDeclare().QueueName;

            _consumer = new EventingBasicConsumer(_channel);
            _consumer.Received += OnReceived;
        }

        public Task<string> CallAsync(string message,
                        CancellationToken cancellationToken = default)
        {
            var correlationId = Guid.NewGuid().ToString();
            var tcs = new TaskCompletionSource<string>();
            _callbackMapper.TryAdd(correlationId, tcs);

            var props = _channel.CreateBasicProperties();
            props.CorrelationId = correlationId;
            props.ReplyTo = _replyQueueName;

            var messageBytes = Encoding.UTF8.GetBytes(message);
            _channel.BasicPublish("", QueueName, props, messageBytes);
            _channel.BasicConsume(consumer: _consumer, queue: _replyQueueName, autoAck: true);

            cancellationToken.Register(() =>
                _callbackMapper.TryRemove(correlationId, out _));
            return tcs.Task;
        }

        public void Close() => _connection.Close();

        private void OnReceived(object model, BasicDeliverEventArgs ea)
        {
            var suchTaskExists = _callbackMapper.TryRemove(ea.BasicProperties.CorrelationId,
                                                                    out var tcs);

            if (!suchTaskExists) return;

            var body = ea.Body.ToArray();
            var response = Encoding.UTF8.GetString(body);

            tcs.TrySetResult(response);

        }
    }
}
