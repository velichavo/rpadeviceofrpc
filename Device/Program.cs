﻿using System;
using System.Collections.Generic;

namespace Device
{
    class Program
    {
        static void Main(string[] args)
        {
            var Lamp = new MainDevice("6c8485b4-f0b2-4f81-b6d7-3b1d87799d15", TypeDevice.Lamp);
            /*var Kettle = new MainDevice("6c8485b4-f0b2-4f81-b6d7-3b1d87799d16", TypeDevice.Kettle);
            Dictionary<string, int> d = new Dictionary<string, int>();
            d.Add("Temperature", 95);
            Kettle.Param = d;*/

            //Kettle.Run();
            Lamp.Run();

            Console.ReadKey();
        }
    }
}
