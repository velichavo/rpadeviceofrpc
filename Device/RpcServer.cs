﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;


namespace Device
{
    public class RpcServer
    {
        private IModel _channel;
        MainDevice device;

        public RpcServer(MainDevice d)
        {
            device = d;
            var factory = new ConnectionFactory()
            {
                HostName = "localhost",
                //Following is required to use AsyncEventingBasicConsumer
                DispatchConsumersAsync = true,
                UserName = "tar",
                Password = "1234"
            };

            using var connection = factory.CreateConnection();
            _channel = connection.CreateModel();

            _channel.QueueDeclare("rpc_queue", false, false, false, null);
            _channel.BasicQos(0, 1, false);

            var consumer = new AsyncEventingBasicConsumer(_channel);
            consumer.Received += OnReceived;

            _channel.BasicConsume("rpc_queue", false, consumer);

            Console.WriteLine(" [x] Awaiting RPC requests");
            Console.WriteLine(" Press [enter] to exit.");
            Console.ReadLine();
        }

        private async Task OnReceived(object model, BasicDeliverEventArgs ea)
        {
            string response = null;

            var body = ea.Body.ToArray();
            var props = ea.BasicProperties;
            var replyProps = _channel.CreateBasicProperties();
            replyProps.CorrelationId = props.CorrelationId;

            try
            {
                var message = Encoding.UTF8.GetString(body);
                Console.WriteLine("Receive: " + message);
                response = await Task.Run(() => device.ExecuteCommand(message));
            }
            catch (Exception e)
            {
                Console.WriteLine(" [.] " + e.Message);
                response = "";
            }
            finally
            {
                var responseBytes = Encoding.UTF8.GetBytes(response);

                _channel.BasicPublish("", props.ReplyTo, replyProps, responseBytes);
                _channel.BasicAck(ea.DeliveryTag, false);
            }
        }
    }
}
