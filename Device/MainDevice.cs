﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Device
{
    public class MainDevice
    {
        private RpcServer rpcServer;
        public TypeDevice TDevice = TypeDevice.Device;
        //брать из настроек приложения
        private Guid id;
        private bool turn;
        private Dictionary<string, int> param = new Dictionary<string, int>();
        private string crc;

        public GetStatusResponse gsr;

        public Guid Id { get => id; set => id = value; }
        public bool Turn { get => turn; set => turn = value; }
        public Dictionary<string, int> Param { get => param; set => param = value; }
        public string Crc { get => crc; set => crc = value; }

        public MainDevice(string guid, TypeDevice td)
        {
            Crc = GetHashCode().ToString();
            Id = new Guid(guid);
            TDevice = td;
        }

        public void Run()
        {
            rpcServer = new RpcServer(this);

        }

        public string GetStatus()
        {
            gsr = new GetStatusResponse();
            gsr.Id = Id;
            gsr.TDevice = TDevice;
            gsr.Param = Param;
            return JsonSerializer.Serialize(gsr, gsr.GetType(), new JsonSerializerOptions(){ WriteIndented = true });
        }

        public string ExecuteCommand(string message)
        {
            MessageRequest mr = JsonSerializer.Deserialize<MessageRequest>(message, new JsonSerializerOptions() { WriteIndented = true});

            switch (mr.Command)
            {
                case TypeCommand.GetStatus:
                    message = GetStatus();
                    break;
                case TypeCommand.Turn:
                    break;
                default:
                    break;
            }

            return message;
        }
    }

    

}
