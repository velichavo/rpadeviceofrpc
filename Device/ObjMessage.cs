﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Device
{
        public class MessageRequest
        {
            private Guid id;
            private TypeCommand command;
            private Dictionary<string, int> param;
            private string crc;
            /// <summary>
            /// ID устройства(регистрируется заранее)
            /// </summary>
            public Guid Id { get => id; set => id = value; }
            /// <summary>
            /// Команда которую будет выполнять устройство(лежит в enum TypeCommand)
            /// </summary>
            public TypeCommand Command { get => command; set => command = value; }
            /// <summary>
            /// парамтры команды(например для комманды Turn у чайника можно задать температуру нагрева воды)
            /// </summary>
            public Dictionary<string, int> Param { get => param; set => param = value; }
            public string Crc { get => crc; set => crc = value; }
            public MessageRequest()
            {
                Crc = GetHashCode().ToString();
            }
        }

        public class GetStatusResponse
        {
            private TypeDevice tDevice;
            private Guid id;
            private Dictionary<string, int> param;

            public GetStatusResponse()
            {

            }
            public Guid Id { get => id; set => id = value; }
            public TypeDevice TDevice { get => tDevice; set => tDevice = value; }
            public Dictionary<string, int> Param { get => param; set => param = value; }
        }


        public enum TypeDevice
        {
            Device,
            Lamp,
            Kettle
        }
        public enum TypeCommand
        {
            GetStatus,
            Turn
        }
}
